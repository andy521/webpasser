package com.hxt.webpasser.regular;

import java.util.List;
import java.util.Map;

import com.hxt.webpasser.transport.xml.Rule;
import com.hxt.webpasser.utils.CommonUtil;

/**
 * 功能说明: 两个数取最大值规则链  <br>
 * 系统版本: v1.0 <br>
 * 开发人员: hanxuetong <br>
 * 审核人员:  <br>
 * 相关文档:  <br>
 * 修改记录:  <br>
 * 修改日期 修改人员 修改说明  <br>
 * ======== ====== ============================================ <br>
 * 
 */
public class MaxRule implements DecideRule{

	public List<Object> handle(Rule rule, List<Object> contentList, Map valueMap) {
		String val=rule.getValue();
		Integer maxInt=CommonUtil.toInteger(val);
		if(contentList!=null&&val!=null&&maxInt!=null)
		{
			
			for(int i=0;i<contentList.size();i++)
			{
				int result =maxInt;
				Integer conVal=CommonUtil.toInteger(contentList.get(i));
				if(conVal!=null){
					if(conVal>maxInt){
						result=conVal;
					}
					
				}
				contentList.set(i, result);
			}	
		}

		return contentList;
	}

}
