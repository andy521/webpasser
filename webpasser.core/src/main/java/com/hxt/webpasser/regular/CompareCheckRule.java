package com.hxt.webpasser.regular;

import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.hxt.webpasser.transport.xml.IfCondition;
import com.hxt.webpasser.transport.xml.IfConditionList;
import com.hxt.webpasser.transport.xml.Rule;
import com.hxt.webpasser.utils.CommonUtil;
import com.hxt.webpasser.utils.FillParamValueUtil;
import com.hxt.webpasser.utils.StringUtil;

/**
 * 功能说明: if判断规则链，如果不满足，则此规则链终止，此字段不返回值  <br>
 * 系统版本: v1.0 <br>
 * 开发人员: hanxuetong <br>
 * 审核人员:  <br>
 * 相关文档:  <br>
 * 修改记录:  <br>
 * 修改日期 修改人员 修改说明  <br>
 * ======== ====== ============================================ <br>
 * 
 */
public class CompareCheckRule  implements DecideRule{
	
	private Logger logger = LoggerFactory.getLogger(getClass());
	
	private String rootContent;
	
	private AcceptDecideRuleHandler acceptDecideRuleHandler;
	
	public CompareCheckRule(String content,AcceptDecideRuleHandler acceptDecideRuleHandler){
		this.rootContent = content;
		this.acceptDecideRuleHandler =acceptDecideRuleHandler;
		
	}

	public List<Object> handle(Rule rule, List<Object> contentList, Map valueMap)  {
		/*
		String leftValueParam=rule.getLeftValue();
		String rightValueParam= rule.getRightValue();
		String operator = rule.getOperator();
		*/
		
		List<Rule> rules = rule.getRules();
		if(rules==null){
			// throw new Exception("if规则链必须要有子Rule");
			logger.error("if规则链必须要有子Rule!");
			return null;
		}
		
		Object orgObject=( contentList==null&&contentList.size()>0)?contentList.get(0):null;
		List<IfConditionList> ifConditionListsItem = rule.getIfConditionLists();
		List<IfCondition> ifConditionsItem = rule.getIfConditions();
		boolean result=false;
		try {
			result = handleIfConditionsOrIfConditionLists(ifConditionListsItem, ifConditionsItem, orgObject, valueMap);
		} catch (Exception e) {
			logger.error(e.getMessage(),e);
		}
		if(result){
			 
			if(rules!=null)
			{
				for(int i=0;i<rules.size();i++)
				{
					Rule ruleItem=rules.get(i);
					DecideRule decideRule=acceptDecideRuleHandler.getDecideRuleByName(ruleItem.getType(),rootContent,i);
					contentList=decideRule.handle(ruleItem, contentList,valueMap);
					
				}
			}
			
		}
		
		return contentList;
	}
	
	/**
	 *  处理 ifConditionList list 或 ifCondition list
	 * @param ifConditionLists
	 * @param ifConditions
	 * @param orgObject
	 * @param valueMap
	 * @return
	 * @throws Exception
	 */
	private boolean handleIfConditionsOrIfConditionLists( List<IfConditionList> ifConditionLists,List<IfCondition> ifConditions,Object orgObject, Map valueMap) throws Exception{
		if(ifConditionLists==null&&ifConditions==null){
			
			throw new Exception("if规则链必须要有ifConditionList或ifCondition!");
		}
		if(ifConditionLists!=null&&ifConditions!=null){
			
			throw new Exception("if规则链同一层级下只能有ifConditionList或ifCondition一种类型!");
		}
		if(ifConditionLists!=null){
			Boolean returnResult = null;
			for(IfConditionList ifConditionList:ifConditionLists){
				List<IfConditionList> ifConditionListsItem = ifConditionList.getIfConditionLists();
				List<IfCondition> ifConditionsItem = ifConditionList.getIfConditions();
				boolean result = handleIfConditionsOrIfConditionLists(ifConditionListsItem,ifConditionsItem, orgObject, valueMap);
				
				String logicOperator = ifConditionList.getLogicOperator();
				returnResult=logicOperatorCombine(logicOperator, returnResult, result);
				
			}
			return returnResult;
			
		}else{
			Boolean returnResult = null;
			for(IfCondition ifCondition:ifConditions){
				boolean result = checkOneIfCondition(ifCondition, orgObject, valueMap);
				
				String logicOperator = ifCondition.getLogicOperator();
				
				returnResult=logicOperatorCombine(logicOperator, returnResult, result);
				
			}
			return returnResult;
		}
		
		
	}
	
	/**
	 * 处理逻辑符， 对 returnResult 赋值
	 * @param logicOperator
	 * @param returnResult
	 * @param result
	 * @throws Exception
	 */
	boolean logicOperatorCombine(String logicOperator ,Boolean returnResult,boolean result) throws Exception{
		
		if(StringUtil.isEmpty(logicOperator)){
			logicOperator="and";
		}
		logicOperator=logicOperator.toLowerCase();
		
		if(returnResult==null){
			returnResult= result;
		}else if("and".equals(logicOperator)){
			returnResult = returnResult&&result;
			
		}else if("or".equals(logicOperator)){
			returnResult = returnResult||result;
			
		}else{
		
			throw new Exception("if规则链logicOperator参数错误!");
		}
		
		return returnResult;
	}
	
	/**
	 * 单个IfCondition 结果
	 * @param ifCondition
	 * @param orgObject
	 * @param valueMap
	 * @return
	 * @throws Exception
	 */
	boolean checkOneIfCondition(IfCondition ifCondition,Object orgObject, Map valueMap) throws Exception{
		String leftValueParam=ifCondition.getLeftValue();
		String rightValueParam= ifCondition.getRightValue();
		String operator = ifCondition.getCompareOperator();
		
		return checkTrue(leftValueParam, rightValueParam, operator, orgObject, valueMap);
	}
	
	/**
	 * 处理 比较运算
	 * @param leftValueParam
	 * @param rightValueParam
	 * @param operator
	 * @param orgObject
	 * @param valueMap
	 * @return
	 * @throws Exception
	 */
	boolean checkTrue(String leftValueParam,String rightValueParam,String operator,Object orgObject, Map valueMap) throws Exception{
		boolean defaultResult= false;
		if(StringUtil.isNotEmpty(leftValueParam)&&StringUtil.isNotEmpty(rightValueParam)&&StringUtil.isNotEmpty(operator)){
			
			Object leftValueOb= FillParamValueUtil.getValueByKeyTemplate(leftValueParam, valueMap, orgObject);
			Object rightValueOb= FillParamValueUtil.getValueByKeyTemplate(rightValueParam, valueMap, orgObject);
			boolean isCheckNull= false;
			if(!"[null]".equals(rightValueOb)){
				if(leftValueOb==null||rightValueOb==null){
					return defaultResult;
				}
			}else{
				
				isCheckNull = true;
			}
			
			if(operator.equals("==")){
				if(isCheckNull){
					if(leftValueOb==null){
						return true;
					}else{
						return false;
					}
				}
				if((leftValueOb+"").equals(rightValueOb+"")){
					
					return true;  
				}else{
					return false; 
				}
				
			}else if(operator.equals("!=")){
				if(isCheckNull){
					if(leftValueOb!=null){
						return true;
					}else{
						return false;
					}
				}
				if(!(leftValueOb+"").equals(rightValueOb+"")){
					
					return true;  
				}else{
					return false; 
				}
				
			}else{
				try {
					double leftValueD = Double.parseDouble(leftValueOb+"");
					double rightValueD = Double.parseDouble(rightValueOb+"");
					
					 if(operator.equals(">")){
							if(leftValueD>rightValueD){
								return true;
							}else{
								return false;
							}
							
					 }else if(operator.equals(">=")){
							if(leftValueD>=rightValueD){
								return true;
							}else{
								return false;
							}
							
					}else if(operator.equals("<")){
						if(leftValueD<rightValueD){
							return true;
						}else{
							return false;
						}
							
					}else if(operator.equals("<=")){
						if(leftValueD<=rightValueD){
							return true;
						}else{
							return false;
						}
						
					}
				} catch (Exception e) {
					throw  e;
				}
				
				
			}
			
			
		}else{
			throw new Exception("if规则链ifCondition参数不全!");
		}
		
		return defaultResult;
	}
	
	

}
