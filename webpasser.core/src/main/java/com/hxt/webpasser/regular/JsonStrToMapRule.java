package com.hxt.webpasser.regular;

import java.util.List;
import java.util.Map;

import com.alibaba.fastjson.JSON;
import com.hxt.webpasser.transport.xml.Rule;
import com.hxt.webpasser.utils.FillParamValueUtil;
import com.hxt.webpasser.utils.StringUtil;

/**
 * 功能说明: json字符串转map <br>
 * 系统版本: v1.0 <br>
 * 开发人员: hanxuetong <br>
 * 审核人员:  <br>
 * 相关文档:  <br>
 * 修改记录:  <br>
 * 修改日期 修改人员 修改说明  <br>
 * ======== ====== ============================================ <br>
 * 
 */
public class JsonStrToMapRule implements DecideRule{

	public List<Object> handle(Rule rule, List<Object> contentList, Map valueMap) {

		//String val=rule.getValue();
		if(contentList!=null)
		{
			for(int i=0;i<contentList.size();i++)
			{
				String con=StringUtil.toString(contentList.get(i));
				if(con!=null){
					Map map = JSON.parseObject(con, Map.class);	
					contentList.set(i, map);
				}
				
				
			}	
		}

		return contentList;
	}

	
	
}
