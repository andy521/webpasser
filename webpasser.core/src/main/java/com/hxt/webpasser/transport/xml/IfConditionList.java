package com.hxt.webpasser.transport.xml;

import java.util.List;

import com.thoughtworks.xstream.annotations.XStreamAlias;
import com.thoughtworks.xstream.annotations.XStreamAsAttribute;
import com.thoughtworks.xstream.annotations.XStreamImplicit;

/**
 * 功能说明:  <br>
 * 系统版本: v1.0 <br>
 * 开发人员: hanxuetong <br>
 * 审核人员:  <br>
 * 相关文档:  <br>
 * 修改记录:  <br>
 * 修改日期 修改人员 修改说明  <br>
 * ======== ====== ============================================ <br>
 * 
 */
@XStreamAlias("ifConditionList")
public class IfConditionList {

	
	@XStreamAsAttribute
	private String logicOperator;
	
	@XStreamImplicit(itemFieldName="ifConditionList")
	private List<IfConditionList> ifConditionLists;
	
	@XStreamImplicit(itemFieldName="ifCondition")
	private List<IfCondition> ifConditions;

	public String getLogicOperator() {
		return logicOperator;
	}

	public void setLogicOperator(String logicOperator) {
		this.logicOperator = logicOperator;
	}

	public List<IfConditionList> getIfConditionLists() {
		return ifConditionLists;
	}

	public void setIfConditionLists(List<IfConditionList> ifConditionLists) {
		this.ifConditionLists = ifConditionLists;
	}

	public List<IfCondition> getIfConditions() {
		return ifConditions;
	}

	public void setIfConditions(List<IfCondition> ifConditions) {
		this.ifConditions = ifConditions;
	}
	
	
	
	
}
