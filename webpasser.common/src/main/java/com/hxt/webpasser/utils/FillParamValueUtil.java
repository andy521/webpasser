/*
 * 系统名称: 
 * 模块名称: webpasser.core
 * 类 名 称: FillParamValueUtil.java
 *   
 */
package com.hxt.webpasser.utils;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

/**
 * 功能说明:  <br>
 * 系统版本: v1.0 <br>
 * 作者: hanxuetong <br>
 * ======== ====== ============================================ <br>
 * 
 */
public class FillParamValueUtil {
	
	//  可以把多个参数替换
	public static String fillParamValue(String con,String reqvals,Map valueMap){
		String orgCon=con;
		List<String> reqFields= getReqFields( reqvals);
		for(String reqField:reqFields)
		{
			if("this".equals(reqField))  //替换上一步的内容
			{
				reqvals=reqvals.replace("[$this]", orgCon);
			}else if(valueMap!=null&&valueMap.containsKey(reqField))
			{
				reqvals=reqvals.replace("[$"+reqField+"]", valueMap.get(reqField)+"");
			}
		}
		return reqvals;
	}
	
	/**
	 * 根据 模版变量字符串 获得对象 如 [$ob.name] [$ob.0.name] ，  只能替换一个
	 * @param reqval
	 * @param valueMap
	 * @param orgObject 上一步对象
	 * @return
	 */
	public static Object getValueByKeyTemplate(String reqval,Object valueMap,Object orgObject){
		String reqField= getReqField( reqval);
		if(StringUtil.isNotEmpty(reqField)){
			
			if(reqField.indexOf(".")==-1){  // [$name] 这种形式
				if("this".equals(reqField)) {
					return orgObject;
				}
				return getValueByKey(reqField, valueMap);
			}else{
				String[] keyFieldsArr=reqField.split("\\.");
				Object localOb=valueMap;
				for(int i=0;i<keyFieldsArr.length;i++){
					String key=keyFieldsArr[i];
					if("this".equals(reqField)) {
						localOb=orgObject;
					}else{
						localOb=getValueByKey(key, localOb);
					}	
				}
				return localOb;
			}
		}
		
		return reqval;
	}
	

	
	@SuppressWarnings("rawtypes")
	private static Object getValueByKey(String key,Object parentObject){
		/*if("this".equals(key)) {
			return orgObject;
		}*/
		if(parentObject==null){
			return null;
		}
		if(parentObject instanceof List){
			Integer point=Integer.parseInt(key);
			List parentList=(List)parentObject;
			return parentList.get(point);
		}else if(parentObject instanceof Map){
			Map parentMap=(Map)parentObject;
			return parentMap.get(key);
		}
		
		return null;
	}
	
	
	/**
	 * 提取需要赋值的字段
	 * @param relateContent
	 * @return
	 */
	protected static List<String> getReqFields(String relateContent)
	{
		List<String> reqItems=new ArrayList<String>();
		if(relateContent!=null)
		{
			String req="\\[\\$([a-zA-Z0-9_.]*)\\]";
			List<String> list=PatternUtils.patternListOne(relateContent, req);
			Iterator<String> it=list.iterator();
			while(it.hasNext())
			{
				
				reqItems.add(it.next());
			}
		}
	
		return reqItems;
	}
	
	
	protected static String getReqField(String reqval)
	{
		if(reqval!=null)
		{
			String req="\\[\\$([a-zA-Z0-9_.]*)\\]";
			List<String> list=PatternUtils.patternListOne(reqval, req);
			Iterator<String> it=list.iterator();
			if(it.hasNext())
			{
				
				return it.next();
			}
		}
	
		return "";
	}
	
	
	
	@SuppressWarnings({ "rawtypes", "unchecked" })
	public static void main(String[] args) {
		Map map=new HashMap();	
		Map itemMap=new HashMap();	
		map.put("itemMap", itemMap);
		itemMap.put("name", "zhangsan");
		List list=new ArrayList();
		list.add("item1");
		list.add("item2");
		itemMap.put("items", list);

		Object ob=getValueByKeyTemplate("[$itemMap.name]", map, null);
		Object ob2=getValueByKeyTemplate("[$itemMap.items.0]", map, null);
		System.out.println(ob);
		System.out.println(ob2);
	}
	
}
